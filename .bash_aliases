alias ls="exa -lha --color=auto --group-directories-first"
# alias ls="ls -lhFA --color=auto --group-directories-first"

alias grep="rg --color=auto"
# alias grep="grep --color=auto"

alias mkdir='function _(){ mkdir -pv -- "$1" && cd -- "$1"; }; _'
alias du="du -ch"
alias df="df -H"

# Dock specifics
alias vpn="cd ~/Documents && sudo openvpn --config dock.ovpn"
alias okta-hml="okta-awscli --okta-profile hml --profile default --force"
alias okta-prd="okta-awscli --okta-profile prd --profile default --force"
